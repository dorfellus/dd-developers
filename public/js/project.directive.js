var myApp = angular.module('myApp');

myApp.directive('project',[function(){
    return{
        restrict: 'EA',
        scope:{
            project: '='
        },
        templateUrl: 'views/project.directive.html',
        controller:function($scope, $location){
        }
    }
}]);