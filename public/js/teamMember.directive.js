var myApp = angular.module('myApp');

myApp.directive('teamMember',[function(){
    return{
        restrict: 'E',
        scope:{
            member: '='
        },
        templateUrl: 'views/teamMember.directive.html',
        controller:function($scope, $location){
        }
    }
}]);
