var myApp = angular.module('myApp', ['ngRoute', 'ui.bootstrap']);

myApp.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/Home', {
            templateUrl: 'views/home.html',
            controller: 'myController'
        })
        .otherwise({
            redirectTo: '/Home'
        })
}]);



myApp.controller('myController', ['$scope',
    function($scope){

        function member(name, title, desc ,img){
            this.name = name;
            this.title = title;
            this.desc = desc;
            this.img = img;
        }

        var dor_c = new member("Dor Cohen", "Senior Software Developer", "", "media/dor.jpeg");
        var dor_f = new member("Dor Fellus-Cohen", "Junior Software Developer", "", "media/dor_f.jpg");

        $scope.teamMembers = [dor_c, dor_f];
        $scope.skills = ["HTML", "CSS","JavaScript", "JQuery", "AngularJS", "Angular7",".NET", "C#", "WebAPI", "WPF", "ASP.NET", "SQL", "Oracle", "SQLServer"];

        function project(name, linkToProj, img){
            this.name = name;
            this.linkToProj = linkToProj;
            this.img = img;
        }


        var fashion = new project("Fashion e-commerce platform", "https://dorothys-closet.firebaseapp.com/#!/Home",
            "media/projects/Fashion_img.png");
        var nursing = new project("Nursing-Care Location System", "https://nursing-care-1530043870004.firebaseapp.com/",
            "media/projects/Nursing_img.png");
        var miniapp = new project("MiniApp", "", "media/projects/MiniApp.png");
        var radlogics = new project("RADLogics", "", "media/projects/Radlogics.png");
        var enghouse = new project("Enghouse Interactive", "", "media/projects/Enghouse.png");

        $scope.portfolio = [fashion, nursing, miniapp, radlogics, enghouse];

        function client(name, logo){
            this.name = name;
            this.logo =logo;
        }

        var ironSource = new client("ironSource", "media/clientsLogos/ironSource.png");
        var lifechiropractic = new client("Life Chiropractic", "media/clientsLogos/Life.png");
        var enghouse = new client("Enghouse", "media/clientsLogos/Enghouse.png");
        var RadLogic = new client("Rad-Logic", "media/clientsLogos/RadLogic.png");

        $scope.clients = [ironSource, lifechiropractic, enghouse, RadLogic];

        $scope.activeNow = 'projects';
        $scope.showProjects = true;
        $scope.showClients = false;

        $scope.changeSelection = function (selection) {
            $scope.activeNow = selection;
            $scope.showProjects = !$scope.showProjects;
            $scope.showClients = !$scope.showClients;
        }

    }
]);